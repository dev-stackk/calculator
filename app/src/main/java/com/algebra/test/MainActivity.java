package com.algebra.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etFirstNumber;
    private EditText etSecondNumber;
    private Button bPlus;
    private Button bMinus;
    private Button bMultiply;
    private Button bDivide;
    private TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();
        setupListeners();
    }

    private void initWidgets() {
        etFirstNumber = findViewById(R.id.etFirstNumber);
        etSecondNumber = findViewById(R.id.etSecondNumber);
        bPlus = findViewById(R.id.bPlus);
        bMinus = findViewById(R.id.bMinus);
        bDivide = findViewById(R.id.bDivide);
        bMultiply = findViewById(R.id.bMultiply);
        tvResult = findViewById(R.id.rezultat);
    }

    private void setupListeners() {
        bPlus.setOnClickListener(view -> {
            add();
        });

        bMinus.setOnClickListener(view -> {
            subtract();
        });

        bMultiply.setOnClickListener(view -> {
            multiply();
        });

        bDivide.setOnClickListener(view -> {
            divide();
        });
    }

    private void add() {
        if (hasUserInput()) {
            int firstNumber = Integer.parseInt(etFirstNumber.getText().toString());
            int secondNumber = Integer.parseInt(etSecondNumber.getText().toString());

            int result = firstNumber + secondNumber;

            tvResult.setText(result + "");
            return;
        }
        Toast.makeText(this, "Invalid values", Toast.LENGTH_LONG).show();
    }

    private void subtract() {
        if (hasUserInput()) {
            int firstNumber = Integer.parseInt(etFirstNumber.getText().toString());
            int secondNumber = Integer.parseInt(etSecondNumber.getText().toString());

            int result = firstNumber - secondNumber;

            tvResult.setText(result + "");
            return;
        }
        Toast.makeText(this, "Invalid values", Toast.LENGTH_LONG).show();
    }

    private void multiply() {
        if (hasUserInput()) {
            int firstNumber = Integer.parseInt(etFirstNumber.getText().toString());
            int secondNumber = Integer.parseInt(etSecondNumber.getText().toString());

            int result = firstNumber * secondNumber;

            tvResult.setText(result + "");
            return;
        }
        Toast.makeText(this, "Invalid values", Toast.LENGTH_LONG).show();
  }

    private void divide() {
        if (hasUserInput()) {

            int firstNumber = Integer.parseInt(etFirstNumber.getText().toString());
            int secondNumber = Integer.parseInt(etSecondNumber.getText().toString());

            double result = (double) firstNumber / (double) secondNumber;

            tvResult.setText(result + "");
            return;
        }
        Toast.makeText(this, "Invalid values", Toast.LENGTH_LONG).show();
    }

    private boolean hasUserInput() {
        if (etFirstNumber.getText().toString().length() > 0 && etSecondNumber.getText().toString().length() > 0) {
            return true;
        }

        return false;
    }

}